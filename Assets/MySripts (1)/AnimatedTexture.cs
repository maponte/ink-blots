﻿using UnityEngine;
using System.Collections;

namespace BoogieDownGames {

	public class AnimatedTexture : MonoBehaviour {

		enum AnimeType { RandVer, RandHoz, RandBoth, SinCos, RandColor, None };

		[SerializeField]
		AnimeType m_animeType = AnimeType.None;

		[SerializeField]
		private string m_textName;

		[SerializeField]
		private float m_speed;

		[SerializeField]
		private float m_offset;

		[SerializeField]
		private float m_min;

		[SerializeField]
		private float m_max;

		delegate void AnimeDelegate();
		AnimeDelegate animeDelegate;


		// Use this for initialization
		void Start () 
		{
			switch( m_animeType ) {
			
			case AnimeType.RandBoth:
				animeDelegate = randBoth;
				break;

			case AnimeType.None:
				animeDelegate = smoothAnime;
				break;

			case AnimeType.SinCos:
				animeDelegate = sinCosAnim;
				break;

			case AnimeType.RandHoz:
				animeDelegate = randomHoz;
				break;

			case AnimeType.RandVer:
				animeDelegate = randVert;
				break;

			case AnimeType.RandColor:

				animeDelegate = setColorAnime;
				break;

			}
		}
		
		// Update is called once per frame
		void Update () 
		{
			animeDelegate();
		}

		void randomHoz()
		{
			m_offset = Random.Range(m_min,m_max);
			renderer.material.SetTextureOffset(m_textName, new Vector2(m_offset, 0));
			renderer.material.SetTextureScale(m_textName, new Vector2(m_offset, 0));
		}

		void randVert()
		{
			m_offset = Random.Range(m_min,m_max);
			renderer.material.SetTextureOffset(m_textName, new Vector2(0,m_offset));
			renderer.material.SetTextureScale(m_textName, new Vector2(0,m_offset));
		}

		void randBoth()
		{
			float x = Random.Range(m_min,m_max);
			float y = Random.Range(m_min,m_max);
			renderer.material.SetTextureOffset(m_textName, new Vector2(x,y));
			renderer.material.SetTextureScale(m_textName, new Vector2(x,y));

		}

		void sinCosAnim()
		{
			float scaleX = Mathf.Cos(Time.time) * m_speed + 1;
			float scaleY = Mathf.Sin(Time.time) * m_speed + 1;

			renderer.material.SetTextureOffset(m_textName, new Vector2(scaleX, scaleY));
			renderer.material.SetTextureScale(m_textName, new Vector2(scaleX, scaleY));
		}

		void smoothAnime()
		{
			//renderer.material.SetTextureOffset(m_textName, new Vector2(scaleX, scaleY));
			//renderer.material.SetTextureScale(m_textName, new Vector2(scaleX, scaleY));
		}

		void setColorAnime()
		{
			Color colo = new Color();
			colo.b = Random.Range(0,255);
			colo.g = Random.Range(0,255);
			colo.r = Random.Range(0,255);
			renderer.material.SetColor(m_textName,colo);
		}
	}
}