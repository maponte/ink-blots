﻿/**
    Mouse Down Event Handler
*/

function Awake() {

  #if UNITY_WEBPLAYER
    gameObject.SetActive (false);
  #endif	
}
function OnMouseDown()
{
    if (this.name == "PlayBT")
    {
        Application.LoadLevel("game");
    }
	else if (this.name == "HelpBT")
	{
    iTween.RotateTo(Camera.main.gameObject, Vector3(0, 90, 0), 0.5);
    }
	else if (this.name == "CreditsBT")
	{
    iTween.RotateTo(Camera.main.gameObject, Vector3(0, -90, 0), 0.5);
    }
	else if ((this.name == "BackHBT") || (this.name == "BackCBT"))
	{
    iTween.RotateTo(Camera.main.gameObject, Vector3(0, 0, 0), 0.5);
    }
    else if (this.name == "ExitBT")
	{
    Application.Quit();
    }
}