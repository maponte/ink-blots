﻿/*
 * 
 * Set the player object tag to Player or else this wont work
 * 
 */

using UnityEngine;
using System.Collections;

namespace BoogieDownGames {

	public class PropPhysics : MonoBehaviour {



		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		 
		}

		void OnTriggerStay(Collider other) 
		{
			if(other.tag == "Player") {
				other.GetComponent<Controls>().m_isColliding = true;
			}
		}

		void OnTriggerExit(Collider other)
		{
			if(other.tag == "Player") { 
				other.GetComponent<Controls>().m_isColliding = false;
			}
		}
	}
}