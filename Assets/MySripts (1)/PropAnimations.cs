﻿using UnityEngine;
using System.Collections;

public class PropAnimations : MonoBehaviour {

	bool CanPlay = true;

	void OnCollisionEnter(Collision collision){

		StartCoroutine (PlayAnim ());
    }

	IEnumerator PlayAnim(){
		if(CanPlay == true){
		CanPlay = false;
		animation.Play ();
		yield return new WaitForSeconds(10);
		CanPlay = true;
		}
	}
}
