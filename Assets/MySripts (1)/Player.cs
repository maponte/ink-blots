﻿/*
 * Player runs the quest and the currentItems
 * 
 */

using UnityEngine;
using System.Collections;

namespace BoogieDownGames {

	public class Player : MonoBehaviour {

		public string m_currentQuest;

		public Item m_currentItem;

		public GameObject m_itemPlacement;

		public int m_questCounter = 0;

		public bool m_isRestart;

		// Use this for initialization
		void Start () {
			m_isRestart = true;
		}
		
		// Update is called once per frame
		void Update () 
		{
			if(m_questCounter >= 12) {
				if(m_isRestart) {
					StartCoroutine(restart());
				}
			}

			if(m_currentItem != null) {
				m_currentItem.transform.position = m_itemPlacement.transform.position;

			}
		
		}

		void OnCollisionEnter(Collision collision)
		{
			if(collision.gameObject.tag == "item") {
				if(m_currentItem == null) {
					m_currentItem = collision.gameObject.GetComponent<Item>();
					m_currentItem.transform.parent = null;
				}
			}
		}

		void OnTriggerEnter(Collider other)  
		{
			if(other.gameObject.tag == "item") {
				if(m_currentItem == null) {
					m_currentItem = other.gameObject.GetComponent<Item>();
					m_currentItem.transform.parent = null;
				}
			}
		}

		void OnCollisionStay(Collision collisionInfo)
		{

		}

		public void destroyItem()
		{
			m_currentItem.transform.parent = null;
			DestroyObject(m_currentItem.gameObject);
			m_currentItem = null;
		}

		public void dropItem()
		{
			if(m_currentItem != null) {
				var f = transform.forward * 6.0f;

				m_currentItem.setParentToWorld(f);
				m_currentItem.transform.eulerAngles = m_currentItem.m_origRotation;
				var p = m_currentItem.transform.position;
				p.y = transform.position.y - 0.5f;
				m_currentItem.transform.position = p;
				m_currentItem = null;


			}
		}

		IEnumerator restart()
		{
			m_isRestart = false;
			yield return new WaitForSeconds(5);
			Application.LoadLevel("menu");
		}

	}
}
