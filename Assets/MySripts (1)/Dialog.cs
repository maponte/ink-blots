﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace BoogieDownGames {
	[Serializable]
	public class Dialog : PropertyAttribute 
	{
		[SerializeField]
		private string m_keyTitle;

		[SerializeField]
		private string m_initial;

		[SerializeField]
		private List<string> m_replyKeys;

		#region PROPERTIES

		public string KeyTitle 
		{
			get { return m_keyTitle; }
			set { m_keyTitle = value; }
		}

		public string Initial
		{
			get { return m_initial; }
			set { m_initial = value; }
		}

		public List<string> ReplyKeys
		{
			get { return m_replyKeys; }
		}



		#endregion



	}
}