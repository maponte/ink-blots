﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace BoogieDownGames 
{

	public class Conversation : MonoBehaviour 
	{
		[SerializeField]
		private string m_CharacterName;

		[SerializeField]
		private string m_inital; //The Initial conversation

		[SerializeField]
		private string m_rightItemReply; //The right item reply

		[SerializeField]
		private string m_wrongItemReply; //The wrong item reply

		[SerializeField]
		private string m_niceGoAwayReply;

		[SerializeField]
		private string m_meanGoAwayMsg;

		[SerializeField]
		private string m_notQuest; //When the quest is not able

		[SerializeField]
		private GameObject m_itemNeeded; //The item needed

		[SerializeField]
		private AudioClip m_intiSound;

		[SerializeField]
		private AudioClip m_rightSound;

		[SerializeField]
		private AudioClip m_wrongSound;

		[SerializeField]
		private AudioClip m_niceGoAwaySound;

		[SerializeField]
		private AudioClip m_meanGoAwaySound;

		[SerializeField]
		private bool m_isInitial;

		[SerializeField]
		private Player m_player;

		[SerializeField]
		private UILabel m_messageBox;

		[SerializeField]
		private bool m_isQuestDone;

		[SerializeField]
		private string m_item;

		public GameObject m_dropItem;


		 

		#region PROPERTIES


		#endregion

		void Start () 
		{
			m_isInitial = true;
		}

		public void listReplies()
		{

		}

		void runWrongItem()
		{
			m_messageBox.text = m_meanGoAwayMsg;
			audio.PlayOneShot(m_meanGoAwaySound);
		}

		void runInitMessage()
		{
			m_messageBox.text = m_inital;
			audio.PlayOneShot(m_intiSound);
			m_isInitial = false;

			if(m_player.m_currentQuest == "") {
				m_player.m_currentQuest = m_CharacterName;
			} else {
				m_isInitial = true;
			}
		}

		void runRightItem()
		{
			m_messageBox.text = m_rightItemReply;
			audio.PlayOneShot(m_rightSound);
			m_isQuestDone = true;
			m_player.m_currentQuest = "";
			m_player.m_questCounter++;
			m_player.destroyItem();

			//drop an item if any
			if(m_dropItem != null) {
				m_dropItem.SetActive(true);
				m_dropItem = null;
			}
		}

		void runQuestDoneAlready()
		{
			m_messageBox.text = m_niceGoAwayReply;
			audio.PlayOneShot(m_niceGoAwaySound);
		}

		void niceGoAway()
		{
			m_messageBox.text = m_niceGoAwayReply;
			audio.PlayOneShot(m_niceGoAwaySound);
		}

		void OnTriggerEnter(Collider other) 
		{

			if(other.tag == "Player") {
				if(other.GetComponent<Controls>().m_isControllable ==true) {
					//Pause the controls make sure player reads the message
					other.GetComponent<Controls>().startPauseControls();

					//First check to see if the player done this quest
					if(m_isQuestDone) {
						runQuestDoneAlready();
					} else {
						if(m_isInitial) {
							runInitMessage();
						} else {
							if(m_player.m_currentQuest == m_CharacterName) {
								if(m_player.m_currentItem == null) {
									runWrongItem();
								} else{
									if(m_player.m_currentItem.m_name == m_item || m_player.m_currentItem.m_quest == m_CharacterName) {
										runRightItem();
									} else {
										runWrongItem();
									}
								}
							} else {
								niceGoAway();
							}
						}
					}
				}

			}
			/*
			if(other.tag == "Player") {

				other.GetComponent<Controls>().startPauseControls();

				if(m_player.m_currentQuest == "" && !m_isQuestDone) {
					if(m_dropItem != null) {
						m_dropItem.SetActive(true);
					}

					m_player.m_currentQuest = m_CharacterName;
					m_messageBox.text = m_inital;
					audio.PlayOneShot(m_intiSound);

				} else if (m_player.m_currentQuest == m_CharacterName){

					if(m_player.m_currentItem != null) {

						if(m_player.m_currentItem.m_name == m_item) {
							m_isQuestDone = true;
							audio.PlayOneShot(m_rightSound);
							m_player.m_currentQuest = "";
							m_messageBox.text = m_rightItemReply;
							m_player.destroyItem();
							m_player.m_questCounter++;

						} else {
							m_messageBox.text = m_wrongItemReply;
							audio.PlayOneShot(m_wrongSound);
						}
					} else if(m_isQuestDone) {
						audio.PlayOneShot(m_niceGoAwaySound);
						m_messageBox.text = m_niceGoAwayReply;
					} else {
						audio.PlayOneShot(m_meanGoAwaySound);
						m_messageBox.text = m_meanGoAwayMsg;
					}
				}
			}
			*/
		}

		void OnTriggerExit(Collider other) 
		{
			m_messageBox.text = "";
		}

	}
}