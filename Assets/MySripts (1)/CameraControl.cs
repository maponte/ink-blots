﻿using UnityEngine;
using System.Collections;


namespace BoogieDownGames {

	public class CameraControl : MonoBehaviour {

		[SerializeField]
		private float m_horSpeed;

		[SerializeField]
		private float m_verSpeed;


		// Use this for initialization
		void Start () 
		{
		
		}
		
		// Update is called once per frame
		void Update () 
		{
		
		}

		void FixedUpdate ()
		{
			float h = m_horSpeed * Input.GetAxis("Mouse X");
			float v = m_verSpeed * Input.GetAxis("Mouse Y");
			transform.Rotate(v, h, 0);
		}
	}
}