﻿using UnityEngine;
using System.Collections;

namespace BoogieDownGames {

	public class Item : MonoBehaviour {

		public string m_name;

		public string m_quest;

		public Vector3 m_origScale;
		public GameObject m_world;
		public Vector3 m_origRotation;

		// Use this for initialization
		void Start () 
		{
			m_origScale = transform.localScale;
			m_origRotation = transform.eulerAngles;
		}
		
		// Update is called once per frame
		void Update () {
		
		}


		public void setParentToWorld(Vector3 p_pos)
		{
			RaycastHit hit;
			if (Physics.Raycast(transform.position, -Vector3.up, out hit)) {

				/*
				float distanceToGround = hit.distance;
				var t = transform.position;
				t.y -= distanceToGround;

				t.z += 1;
				transform.position = t;
				transform.parent = null;
				*/
				transform.Translate(p_pos);
				transform.parent = m_world.transform;
				//transform.localScale = m_origScale;
			}
		}
	}
}