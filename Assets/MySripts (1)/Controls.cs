﻿/*
 *This scripts goes on the Ui Root i.e player
 *So place it on it
 *set the bounce rate to what you want the player will bounce of the 
 *props
 */

using UnityEngine;
using System.Collections;

namespace BoogieDownGames {

	public class Controls : MonoBehaviour {

		[SerializeField]
		private float m_maxDistanceFromGround;

		[SerializeField]
		private float m_rotSpeed;

		[SerializeField]
		private float m_vertSpeed;

		[SerializeField]
		private GameObject m_world;

		public float m_vert;

		public float m_horz;

		public bool m_isColliding;

		public float m_bounceRate;

		public float m_vClampMin;
		public float m_vClampMax;

		public Vector3 m_iniPos;

		public bool m_isControllable = true;

		public float m_howLongtoPauseInConvo;

		public GameObject m_fromVector;

		// Use this for initialization
		void Start () 
		{
			m_iniPos = transform.position;
			m_isControllable = true;
		}

		public void nonCollideControls()
		{
			m_horz = Input.GetAxis("Horizontal");
			m_vert = Input.GetAxis("Vertical");
			m_world.transform.Rotate(transform.up, m_rotSpeed * Time.deltaTime * -m_horz, Space.World);
			m_world.transform.Rotate(transform.right, m_vertSpeed * Time.deltaTime * m_vert, Space.World);
			//m_world.transform.Rotate(m_fromVector.transform.up, m_rotSpeed * Time.deltaTime * -m_horz, Space.World);
			//m_world.transform.Rotate( m_fromVector.transform.right, m_vertSpeed * Time.deltaTime * m_vert, Space.World);
		}
		public void collidedControls()
		{
			m_horz = Input.GetAxis("Horizontal") * -m_bounceRate;
			m_vert = Input.GetAxis("Vertical") * -m_bounceRate;
			m_world.transform.Rotate(transform.up, m_rotSpeed * Time.deltaTime * -m_horz, Space.World);
			m_world.transform.Rotate(transform.right, m_vertSpeed * Time.deltaTime * m_vert, Space.World);
			//m_world.transform.Rotate( m_fromVector.transform.right, m_vertSpeed * Time.deltaTime * m_vert, Space.World);
		}
		
		// Update is called once per frame
		void Update () 
		{
			if(m_isControllable) {

				if(Input.GetButton("Fire1") ) {
					GetComponent<Player>().dropItem();
				}
				if(m_isColliding) {
					collidedControls();
				} else {
					nonCollideControls();
				}
			} else {
				if(Input.GetButton("Fire2") ) {
					m_isControllable = true;
				}
			}

			if( transform.position != m_iniPos) {
				
				transform.position = m_iniPos;
			}

			if(Input.GetKey(KeyCode.Escape) || Input.GetButton("Fire3")) {
				Application.LoadLevel("menu");;
			}

		}

		void FixedUpdate()
		{

		}



		IEnumerator pauseControls(float p_sec) 
		{
			m_isControllable = false;
			yield return new WaitForSeconds(p_sec);
			m_isControllable = true;
		}

		public void startPauseControls()
		{
			StartCoroutine(pauseControls(m_howLongtoPauseInConvo));
		}

	}
}