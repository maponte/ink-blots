﻿using UnityEngine;
using System.Collections;

namespace BoogieDownGames {

	public class SphereControl : MonoBehaviour {

		enum RotationType { World, Local, Orientaion };

		[SerializeField]
		private RotationType m_rotType = RotationType.World;

		[SerializeField]
		private GameObject m_orietation;

		[SerializeField]
		private float m_yRotSpeed;

		[SerializeField]
		private float m_zRotSpeed;

		[SerializeField]
		private float m_yRot;

		[SerializeField]
		private float m_zRot;

		delegate void RotDel();
		RotDel rotDel;
		
		// Use this for initialization
		void Start () 
		{
			switch( m_rotType ) {
			case RotationType.Local:
				rotDel = rotateByWorldAxis;
				break;

			case RotationType.Orientaion:
				rotDel = rotateByOrientation;
				break;

			case RotationType.World:
				rotDel = rotateByLocalAxis;
				break;
			}

		}
		
		// Update is called once per frame
		void Update () 
		{

		}

		void FixedUpdate ()
		{
			rotDel();
		}

		public void rotateByWorldAxis()
		{
			m_yRot = m_yRotSpeed * Input.GetAxis("Horizontal");
			transform.Rotate( new Vector3(0,m_yRot,0),Space.World);
			
			m_zRot = m_zRotSpeed * Input.GetAxis("Vertical");
			transform.Rotate(Vector3.left * m_zRot,Space.World);
		}

		public void rotateByLocalAxis()
		{
			m_yRot = m_yRotSpeed * Input.GetAxis("Horizontal");
			transform.Rotate( new Vector3(0,m_yRot,0));
			
			m_zRot = m_zRotSpeed * Input.GetAxis("Vertical");
			transform.Rotate(Vector3.left * m_zRot);
		}

		public void rotateByOrientation ()
		{
			//float speed = m_zRotSpeed * Time.deltaTime * Input.GetAxis("Vertical");
			//transform.Rotate( speed * m_orietation.transform.eulerAngles.x,speed * m_orietation.transform.eulerAngles.y,
			                 //speed * m_orietation.transform.eulerAngles.z);

			//transform.LookAt(m_orietation.transform.position - transform.position);

			m_yRot = m_yRotSpeed * Input.GetAxis("Mouse X");


		}
	}
}