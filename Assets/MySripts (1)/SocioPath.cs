﻿using UnityEngine;
using System.Collections;


namespace BoogieDownGames {

	public class SocioPath : MonoBehaviour {


		[SerializeField]
		private UILabel m_messageBox;

		public AudioClip m_audioOne;
		public AudioClip m_audioTwo;
		public AudioClip m_audioThree;


		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () 
		{
		
		}

		void OnTriggerEnter(Collider other) 
		{
			if(other.tag == "Player") {
				other.GetComponent<Controls>().startPauseControls();
				Player p = other.GetComponent<Player>();
				if(p.m_questCounter <= 5) {
					m_messageBox.text = "What are you looking at?  I don't need anything from you, go away!";
					audio.PlayOneShot(m_audioOne);

				} else if (p.m_questCounter >= 6 && p.m_questCounter <= 10) {
					m_messageBox.text = "You found me again, you know there's no purpose to seeing me, you know that?";
					audio.PlayOneShot(m_audioTwo);
				} else {
					m_messageBox.text = "Wow, you found me, I found you, we found us, they found they, good for me, good for you, good for them, good for us!";
					audio.PlayOneShot(m_audioThree);
					p.m_questCounter = 13;
				}
			}
		}

		void OnTriggerExit(Collider other) 
		{
			m_messageBox.text = "";
		}
	}

}
